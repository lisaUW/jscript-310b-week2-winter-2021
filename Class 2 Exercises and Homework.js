// 1. Create an object representation of yourself
// Should include: 
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)
let meObject = { "firstName":"Lisa", "age":36, "favoriteFood":"Ethiopian", "bestFriendObject": {"firstName": "Elisa", 'age': 35, 'favoriteFood': "French Cuisine"}};

// 2. console.log best friend's firstName and your favorite food
console.log(meObject.bestFriendObject.firstName); 
console.log(meObject.favoriteFood);

// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X
let ttt = [
    ["-", 0, "-"],
    ["-", "X", 0],
    ["X", "-", "X"]
];



// 4. After the array is created, 'O' claims the top right square.
// Update that value.
ttt [0][2] = 0;

// 5. Log the grid to the console.
console.log(ttt);

// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test

const emailRegex = new RegExp('.+@.+\..+');

// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date
const assignmentDate = '1/21/2019';
let numericalDate = new Date(Date.parse(assignmentDate));


// 8. Create a new Date instance to represent the dueDate.  
// This will be exactly 7 days after the assignment date.
 let dueDate = numericalDate
 
dueDate.setDate(dueDate.getDate() + 7);



// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help

//<time datetime="YYYY-MM-DD">Month day, year</time>
 
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];


let month = dueDate.getMonth(); 
let monthName = months[month];
let day = dueDate.getDay();
let year = dueDate.getFullYear();
let monthNumber = month + 1; 

let timeTag = `<time datetime="${year}-${monthNumber}-${day}">${monthName} ${day}, ${year}</time>`


// 10. log this value using console.log
console.log(timeTag);